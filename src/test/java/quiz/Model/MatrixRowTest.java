package quiz.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MatrixRowTest {

    private MatrixRow row ;
    private String inputData = "1 2 3 4\n";

    @BeforeEach
    public void init(){
        row = new MatrixRow(inputData);
    }

    @Test
    public void testRowInitialization(){
        int[] expectedData = new int[] {1, 2, 3, 4};
        int[] parsedRowData = row.getData();

        assertEquals(expectedData.length, parsedRowData.length);
        for(int i = 0; i < expectedData.length; i++) {
            assertEquals(expectedData[i], parsedRowData[i]);
        }
    }

    @Test
    public void testWholeRowSum(){
        int expectedValue = 10;
        long result = row.getSumBetweenIndexes(0, row.getRowLenght()-1);
        assertEquals(expectedValue, result);
    }

    @Test
    public void testPartlyRowSum() {
        int expectedValue = 5;
        long result = row.getSumBetweenIndexes(1, 2);
        assertEquals(expectedValue, result);
    }

    @Test
    public void testSumRowWithEqualsIndexes() {
        int expectedValue = 1;
        long result = row.getSumBetweenIndexes(0, 0);
        assertEquals(expectedValue, result);
    }
}
