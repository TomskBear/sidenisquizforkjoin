package quiz.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import quiz.Engine.MatrixCreator;
import quiz.Engine.TaskDataHandler;
import quiz.Model.Exceptions.IncorrectQueryTypeException;
import quiz.Model.Exceptions.SizeMismatchException;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculateQueryTest {

    private Matrix internalMatrix;

    @BeforeEach
    public void initMatrix() throws SizeMismatchException {
        String data = "4\n" +
                "-1 -5 1 2\n" +
                "1 2 3 4\n" +
                "5 6 7 8\n" +
                "9 10 11 12\n" +
                "5\n" +
                "1 0 0 0 0\n" +
                "1 1 1 2 1\n" +
                "2 0 0 2\n" +
                "1 0 0 0 0\n" +
                "1 0 0 3 3";

        System.setIn(new ByteArrayInputStream(data.getBytes()));
        TaskDataHandler handler = new TaskDataHandler();
        TaskData taskData = handler.getInputTaskData(System.in);
        MatrixCreator creator = new MatrixCreator(taskData);
        internalMatrix = creator.createMatrix();
    }

    @Test
    public void testSum() throws IncorrectQueryTypeException {
        String expectedResult = "5";
        CalculateSumQuery query = new CalculateSumQuery(0,"1 1 1 2 1");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        query.setCurrentMatrix(internalMatrix);
        String result = query.execute();
        //assertEquals(expectedResult, baos.toString().replaceAll("(\\r|\\n)", ""));
        assertEquals(expectedResult, result);
    }
}
