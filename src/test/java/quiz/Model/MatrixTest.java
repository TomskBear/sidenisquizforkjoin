package quiz.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import quiz.Engine.MatrixCreator;
import quiz.Engine.TaskDataHandler;
import quiz.Model.Exceptions.SizeMismatchException;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class MatrixTest {

    private Matrix internalMatrix;

    @BeforeEach
    public void initMatrix() throws SizeMismatchException {
        String data = "4\n" +
                "-1 -5 1 2\n" +
                "1 2 3 4\n" +
                "5 6 7 8\n" +
                "9 10 11 12\n" +
                "5\n" +
                "1 0 0 0 0\n" +
                "1 1 1 2 1\n" +
                "2 0 0 2\n" +
                "1 0 0 0 0\n" +
                "1 0 0 3 3";

        System.setIn(new ByteArrayInputStream(data.getBytes()));
        TaskDataHandler handler = new TaskDataHandler();
        TaskData taskData = handler.getInputTaskData(System.in);
        MatrixCreator creator = new MatrixCreator(taskData);
        internalMatrix = creator.createMatrix();
    }

    @Test
    public void testSumNeibours(){
        int expectedValue = 5;
        long result = internalMatrix.sumSubMatrix(1,1,1,2);
        assertEquals(expectedValue, result);
    }

    @Test
    public void testInitedValues() throws SizeMismatchException {
        Matrix expectedMatrix = new Matrix(4);

        expectedMatrix.addRow(new MatrixRow(new int[]{-1, -5, 1, 2}));
        expectedMatrix.addRow(new MatrixRow(new int[]{1, 2, 3, 4}));
        expectedMatrix.addRow(new MatrixRow(new int[]{5, 6, 7, 8}));
        expectedMatrix.addRow(new MatrixRow(new int[]{9, 10, 11, 12}));

        assertEquals(expectedMatrix, internalMatrix);
    }

    @Test
    public void testValueModification() throws SizeMismatchException {
        Matrix expectedMatrix = new Matrix(4);

        expectedMatrix.addRow(new MatrixRow(new int[]{-1, -5, 1, 2}));
        expectedMatrix.addRow(new MatrixRow(new int[]{1, 2, 8, 4}));
        expectedMatrix.addRow(new MatrixRow(new int[]{5, 6, 7, 8}));
        expectedMatrix.addRow(new MatrixRow(new int[]{9, 10, 11, 12}));

        internalMatrix.modifyValue(2,1, 8);

        assertEquals(expectedMatrix, internalMatrix);
    }

    @Test
    public void testSumMatrixOnOnePosition(){
        int expectedValue = -1;
        long result = internalMatrix.sumSubMatrix(0,0,0,0);
        assertEquals(expectedValue, result);
    }

    @Test
    public void testSumWholeMatrix(){
        int expectedValue = 75;
        long result = internalMatrix.sumSubMatrix(0,0,3,3);
        assertEquals(expectedValue, result);
    }
}
