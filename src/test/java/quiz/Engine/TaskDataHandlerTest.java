package quiz.Engine;

import org.junit.jupiter.api.Test;
import quiz.Model.TaskData;
import java.io.ByteArrayInputStream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TaskDataHandlerTest {

    @Test
    public void simpleTest(){
        String data = "4\n" +
                "-1 -5 1 2\n" +
                "1 2 3 4\n" +
                "5 6 7 8\n" +
                "9 10 11 12\n" +
                "5\n" +
                "1 0 0 0 0\n" +
                "1 1 1 2 1\n" +
                "2 0 0 2\n" +
                "1 0 0 0 0\n" +
                "1 0 0 3 3";

        System.setIn(new ByteArrayInputStream(data.getBytes()));
        TaskDataHandler handler = new TaskDataHandler();
        TaskData taskdata = handler.getInputTaskData(System.in);
        assertEquals(taskdata.getMatrixInputData().size(), 4);
        assertEquals(taskdata.getQueriesInputData().size(), 5);
    }
}
