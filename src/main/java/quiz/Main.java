package quiz;

import javafx.util.Pair;
import quiz.Engine.MatrixCreator;
import quiz.Engine.QueriesListCreator;
import quiz.Engine.TaskDataHandler;
import quiz.Model.AbstractQuery;
import quiz.Model.Exceptions.IncorrectQueryTypeException;
import quiz.Model.Exceptions.SizeMismatchException;
import quiz.Model.Matrix;
import quiz.Model.QueryType;
import quiz.Model.TaskData;
import quiz.Threading.CalculationThread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ForkJoinPool;

public class Main {
    private static int THREEASHOLD = 1000;

    public static void main(String[] args) throws IOException, IncorrectQueryTypeException, CloneNotSupportedException, InterruptedException {
        TaskDataHandler handler = new TaskDataHandler();
        TaskData handledData = handler.getInputTaskData(System.in);
        MatrixCreator creator = new MatrixCreator(handledData);
        Matrix matrix;
        try {
            matrix = creator.createMatrix();
        } catch (SizeMismatchException e) {
            e.printStackTrace();
            return;
        }
        QueriesListCreator queriesListCreator = new QueriesListCreator(handledData);
        List<AbstractQuery> queries = queriesListCreator.getQueriesList();
        List<AbstractQuery> queryForCalculations = new ArrayList<>();
        for (AbstractQuery item: queries) {
            if (item.getQueryType() == QueryType.Calculate) {
                item.setCurrentMatrix((Matrix) matrix.clone());
                queryForCalculations.add(item);
            }
            if (item.getQueryType() == QueryType.Modify) {
                item.setCurrentMatrix(matrix);
                item.execute();
            }
        }
        ConcurrentSkipListMap<Integer,String> results = new ConcurrentSkipListMap<>();
        ForkJoinPool pool = new ForkJoinPool();
        CalculationThread calculationThread = new CalculationThread(results,0, queryForCalculations.size(),queryForCalculations);
        pool.execute(calculationThread);
        calculationThread.join();
        while(!calculationThread.isDone()){
            Thread.sleep(10);
        }
        for(Map.Entry<Integer, String> entry : results.entrySet()){
            System.out.println(entry.getValue());
        }
    }

}


