package quiz.Engine;

import quiz.Model.TaskData;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskDataHandler {

    private final int minMatrixRowsNumber = 1;
    private final int maxMatrixRowsNumber  = 550;

    private final int minQueriesNumber = 0;
    private final int maxQueriesNumber = 100100;

    private boolean isMatrixRowsQuantityCorrect(int currentRowsQuantity){
        return minMatrixRowsNumber <= currentRowsQuantity && currentRowsQuantity <= maxMatrixRowsNumber;
    }

    private boolean isQueriesQuantityCorrect(int queriesQuantity){
        return minQueriesNumber <= queriesQuantity && queriesQuantity<= maxQueriesNumber;
    }

    private List<String> getDataList(Scanner scanner, int currentRowsQuantity) {
        List<String> result = new ArrayList<>();
        int i = 0;
        while(i < currentRowsQuantity) {
            if (scanner.hasNextLine()) {
                result .add(scanner.nextLine());
            }
            i++;
        }
        return result;
    }

    public TaskData getInputTaskData(InputStream inputDataStream){
        TaskData handledData = new TaskData();
        Scanner internalScanner = new Scanner(inputDataStream);
        int matrixColumnsQuantity = internalScanner.nextInt();
        handledData.setMatrixSize(matrixColumnsQuantity);
        internalScanner.nextLine();
        if (isMatrixRowsQuantityCorrect(matrixColumnsQuantity)) {
            handledData.setMatrixInputData(getDataList(internalScanner, matrixColumnsQuantity));
        }
        int queriesQuantity = internalScanner.nextInt();
        internalScanner.nextLine();
        if (isQueriesQuantityCorrect(queriesQuantity)) {
            handledData.setQueriesInputData(getDataList(internalScanner, queriesQuantity));
        }
        return handledData;
    }
}
