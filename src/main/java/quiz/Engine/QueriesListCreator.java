package quiz.Engine;

import quiz.Model.*;
import quiz.Model.Exceptions.IncorrectQueryTypeException;

import java.util.ArrayList;
import java.util.List;

public class QueriesListCreator {
    private int calculateQueryNumber;
    private final List<String> inputQueries;

    public QueriesListCreator(TaskData incomingData)
    {
        calculateQueryNumber = 0;
        inputQueries = incomingData.getQueriesInputData();
    }

    public List<AbstractQuery> getQueriesList() throws IncorrectQueryTypeException {
        List<AbstractQuery> result = new ArrayList<>();
        for (String data: inputQueries) {
            String queryTypeSymbol = data.substring(0,1);
            switch (queryTypeSymbol) {
                case "1":
                    result.add(new CalculateSumQuery(calculateQueryNumber++, data));
                    break;
                case "2":
                    result.add(new ModifyElementQuery(data));
                    break;
            }
        }
        return result;
    }
}
