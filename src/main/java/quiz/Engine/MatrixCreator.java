package quiz.Engine;

import quiz.Model.Exceptions.SizeMismatchException;
import quiz.Model.Matrix;
import quiz.Model.MatrixRow;
import quiz.Model.TaskData;

import java.util.List;

public class MatrixCreator {
    private final List<String> inputMatrixData;
    private int matrixSize;

    public MatrixCreator(TaskData data) {
        this.inputMatrixData = data.getMatrixInputData();
        this.matrixSize = data.getMatrixSize();
    }

    public Matrix createMatrix() throws SizeMismatchException {
        Matrix matrix = new Matrix(matrixSize);
        for (String data: inputMatrixData) {
            matrix.addRow(new MatrixRow(data));
        }
        return matrix;
    }
}
