package quiz.Threading;

import quiz.Model.AbstractQuery;
import quiz.Model.QueryType;

import java.util.List;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.RecursiveAction;

public class CalculationThread extends RecursiveAction {
    private List<AbstractQuery> queries;
    private ConcurrentSkipListMap<Integer,String> listForResults;
    private Integer start;
    private Integer end;
    private final Integer THRESHOLD = 1000;

    public CalculationThread(ConcurrentSkipListMap<Integer,String> listForResults, Integer start, Integer end, List<AbstractQuery> queries) {
        this.start= start;
        this.end = end;
        this.listForResults = listForResults;
        this.queries = queries;
    }

    @Override
    public void compute() {
        if (end - start > THRESHOLD) {
            int middle = (end + start) / 2;

            CalculationThread firstThread = new CalculationThread(listForResults, start, middle, queries);
            CalculationThread secondThread = new CalculationThread(listForResults, middle + 1, end, queries);

            firstThread .fork();
            secondThread.fork();

            secondThread.join();
            firstThread.join();
        } else  {
            for (AbstractQuery query : queries) {
                if (query.getQueryType() == QueryType.Calculate) {
                    listForResults.put(query.getQueryNumber(),query.execute()) ;
                }
            }
        }
    }
}
