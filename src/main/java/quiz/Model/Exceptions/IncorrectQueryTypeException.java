package quiz.Model.Exceptions;

public class IncorrectQueryTypeException extends Exception {
    public IncorrectQueryTypeException() {
        super("Incorrect query type");
    }
}
