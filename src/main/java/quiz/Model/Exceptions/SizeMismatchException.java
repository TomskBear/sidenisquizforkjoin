package quiz.Model.Exceptions;

public class SizeMismatchException extends Exception{
    public SizeMismatchException(int matrixSize, int rowLenght) {
        super("Matrix and row size are not the same. Matrix size is = " + matrixSize + " and row length is " + rowLenght);
    }
}
