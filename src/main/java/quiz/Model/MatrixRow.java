package quiz.Model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.IntStream;

public class MatrixRow {
    private int[] internalData;

    private HashMap<IndexesTuple, Long> sumResults;

    public MatrixRow() {
        sumResults = new HashMap<>();
    }

    public MatrixRow(String inputData)
    {
        this();
        initRow(inputData);
    }

    public MatrixRow(int[] data)
    {
        this();
        internalData = data;
    }

    private void initRow(String inputData) {
        inputData = inputData.trim();
        String[] splittedData = inputData.split(" ");
        internalData = new int[splittedData.length];
        for (int i = 0; i < splittedData.length; i++) {
            internalData[i] = Integer.parseInt(splittedData[i]);
        }
    }

    public MatrixRow clone(){
        return new MatrixRow(Arrays.copyOf(internalData, internalData.length));
    }

    public int[] getData(){
        return internalData;
    }

    public int getValueAt(int index)
    {
        return internalData[index];
    }

    public void setNewValue(int index, int newValue)
    {
        internalData[index] = newValue;
        sumResults.clear();
    }

    public int getRowLenght(){
        if (internalData != null) {
            return internalData.length;
        }
        return 0;
    }

    public long getSumBetweenIndexes(int startIndex, int endIndex) {
        IndexesTuple tuple = new IndexesTuple(startIndex, endIndex);
        if (sumResults.containsKey(tuple)) {
            return sumResults.get(tuple);
        }
        long result = 0;
        for(int i = startIndex; i <=endIndex; i++) {
            result += internalData[i];
        }
        sumResults.put(tuple, result);
        return result;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatrixRow row = (MatrixRow) o;
        return Arrays.equals(internalData, row.internalData);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(internalData);
    }
}
