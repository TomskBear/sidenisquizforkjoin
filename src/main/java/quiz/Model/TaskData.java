package quiz.Model;

import java.util.List;

public class TaskData {
    private List<String> matrixInputData;
    private List<String> queriesInputData;

    private int matrixSize;

    public int getMatrixSize() {
        return matrixSize;
    }

    public void setMatrixSize(int matrixSize) {
        this.matrixSize = matrixSize;
    }

    public List<String> getMatrixInputData() {
        return matrixInputData;
    }

    public void setMatrixInputData(List<String> matrixInputData) {
        this.matrixInputData = matrixInputData;
    }

    public List<String> getQueriesInputData() {
        return queriesInputData;
    }

    public void setQueriesInputData(List<String> queriesInputData) {
        this.queriesInputData = queriesInputData;
    }
}
