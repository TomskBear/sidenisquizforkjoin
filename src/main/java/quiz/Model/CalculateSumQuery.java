package quiz.Model;

import quiz.Model.Exceptions.IncorrectQueryTypeException;

public class CalculateSumQuery extends AbstractQuery {
    private int startRowIndex;
    private int startColumnIndex;
    private int endRowIndex;
    private int endColumnIndex;

    public CalculateSumQuery(int queryNumber, String inputData) throws IncorrectQueryTypeException {
        setQueryNumber(queryNumber);
        parseInputData(inputData);
    }

    public QueryType getQueryType(){
        return QueryType.Calculate;
    }

    public CalculateSumQuery(int startRowIndex, int startColumnIndex, int endRowIndex, int endColumnIndex) {
        this.startRowIndex = startRowIndex;
        this.startColumnIndex = startColumnIndex;
        this.endRowIndex = endRowIndex;
        this.endColumnIndex = endColumnIndex;
    }

    private void parseInputData(String inputData) throws IncorrectQueryTypeException {
        String[] splittedData = inputData.trim().split(" ");
        if (!splittedData[0].equals("1")) {
            throw new IncorrectQueryTypeException();
        }

        this.startRowIndex = Integer.parseInt(splittedData[2]);
        this.startColumnIndex = Integer.parseInt(splittedData[1]);
        this.endRowIndex = Integer.parseInt(splittedData[4]);
        this.endColumnIndex = Integer.parseInt(splittedData[3]);
    }

    @Override
    public String execute() {
        return String.valueOf(getInternalMatrix().sumSubMatrix(startRowIndex, startColumnIndex, endRowIndex, endColumnIndex));
    }
}
