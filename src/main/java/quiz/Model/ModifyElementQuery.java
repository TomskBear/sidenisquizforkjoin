package quiz.Model;

import quiz.Model.Exceptions.IncorrectQueryTypeException;

public class ModifyElementQuery extends AbstractQuery {
    private int rowIndex;
    private int columnIndex;
    private int newValue;

    public ModifyElementQuery(int rowIndex, int columnIndex, int value) {
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
        this.newValue = value;
    }

    public QueryType getQueryType(){
        return QueryType.Modify;
    }

    public ModifyElementQuery(String inputData) throws IncorrectQueryTypeException {
        parseInputData(inputData);
    }

    private void parseInputData(String inputData) throws IncorrectQueryTypeException {

        String[] splittedData = inputData.trim().split(" ");
        if (!splittedData[0].equals("2")) {
            throw new IncorrectQueryTypeException();
        }
        this.rowIndex = Integer.parseInt(splittedData[1]);
        this.columnIndex = Integer.parseInt(splittedData[2]);
        this.newValue = Integer.parseInt(splittedData[3]);
    }

    @Override
    public String execute() {
        getInternalMatrix().modifyValue(rowIndex, columnIndex, newValue);return "";
    }
}
