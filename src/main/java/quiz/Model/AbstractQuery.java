package quiz.Model;

public abstract class AbstractQuery {
    private Matrix internalMatrix;
    public abstract String execute();
    public abstract QueryType getQueryType();
    private Integer queryNumber;

    public AbstractQuery() {
        queryNumber = 0;
    }

    public void setCurrentMatrix(Matrix matrix){
        internalMatrix = matrix;
    }

    public Matrix getInternalMatrix() {
        return internalMatrix;
    }

    protected void setQueryNumber(Integer value) {
        queryNumber = value;
    }

    public Integer getQueryNumber(){
        return queryNumber;
    }
}
