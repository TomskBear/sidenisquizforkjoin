package quiz.Model;

import quiz.Model.Exceptions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Matrix{
    private HashMap<IndexesQuad, Long> calculatedQueries;

    private List<MatrixRow> rowsList;
    private int matrixSize;

    public Matrix() {
        calculatedQueries = new HashMap<>();
    }

    public Matrix(int matrixSize) {
        this();
        rowsList = new ArrayList<>();
        this.matrixSize = matrixSize;
    }

    public void addRow(MatrixRow newRow) throws SizeMismatchException {
        if (newRow.getRowLenght() != matrixSize) {
            throw new SizeMismatchException(matrixSize, newRow.getRowLenght());
        }
        rowsList.add(newRow);
    }

    public Matrix clone(){
        Matrix newMatrix = new Matrix(matrixSize);
        for (MatrixRow row: rowsList) {
            try {
                newMatrix.addRow(row.clone());
            } catch (SizeMismatchException e) {
                e.printStackTrace();
            }
        }
        return newMatrix;
    }

    public void modifyValue(int rowNumber, int columnNumber, int newValue) {
        MatrixRow row = rowsList.get(columnNumber);
        row.setNewValue(rowNumber, newValue);
        calculatedQueries.clear();
    }

    public long sumSubMatrix(int startRowNumber, int startColumnNumber, int endRowNumber, int endColumnNumber ) {
        IndexesQuad quad = new IndexesQuad(startRowNumber, startColumnNumber,endRowNumber, endColumnNumber);
        if (calculatedQueries.containsKey(quad)) {
            return calculatedQueries.get(quad);
        }
        long result = 0;
        for (int i = startRowNumber; i <= endRowNumber; i++) {
            result += rowsList.get(i).getSumBetweenIndexes(startColumnNumber, endColumnNumber);
        }
        calculatedQueries.put(quad, result);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return matrixSize == matrix.matrixSize &&
                Objects.equals(rowsList, matrix.rowsList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rowsList, matrixSize);
    }
}
