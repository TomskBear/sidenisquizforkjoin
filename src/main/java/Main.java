import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class Main {

    public static class IncorrectQueryTypeException extends Exception {
        public IncorrectQueryTypeException() {
            super("Incorrect query type");
        }
    }

    public static class SizeMismatchException extends Exception{
        public SizeMismatchException(int matrixSize, int rowLenght) {
            super("Matrix and row size are not the same. Matrix size is = " + matrixSize + " and row length is " + rowLenght);
        }
    }

    public static abstract class AbstractQuery {
        private Matrix internalMatrix;
        public abstract String execute();
        public abstract QueryType getQueryType();
        private Integer queryNumber;

        public void setCurrentMatrix(Matrix matrix){
            internalMatrix = matrix;
        }

        public Matrix getInternalMatrix() {
            return internalMatrix;
        }

        protected void setQueryNumber(Integer value) {
            queryNumber = value;
        }

        public Integer getQueryNumber(){
            return queryNumber;
        }
    }

    public static class IndexesQuad {
        private int x1;
        private int y1;
        private int x2;
        private int y2;

        public IndexesQuad(int x1, int y1, int x2, int y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
        }

        public int getX1() {
            return x1;
        }

        public void setX1(int x1) {
            this.x1 = x1;
        }

        public int getY1() {
            return y1;
        }

        public void setY1(int y1) {
            this.y1 = y1;
        }

        public int getX2() {
            return x2;
        }

        public void setX2(int x2) {
            this.x2 = x2;
        }

        public int getY2() {
            return y2;
        }

        public void setY2(int y2) {
            this.y2 = y2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            IndexesQuad that = (IndexesQuad) o;
            return x1 == that.x1 &&
                    y1 == that.y1 &&
                    x2 == that.x2 &&
                    y2 == that.y2;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x1, y1, x2, y2);
        }
    }

    public static class IndexesTuple {
        private int startIndex;
        private int endIndex;

        public IndexesTuple(int startIndex, int endIndex) {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
        }

        public int getStartIndex() {
            return startIndex;
        }

        public void setStartIndex(int startIndex) {
            this.startIndex = startIndex;
        }

        public int getEndIndex() {
            return endIndex;
        }

        public void setEndIndex(int endIndex) {
            this.endIndex = endIndex;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            IndexesTuple that = (IndexesTuple) o;
            return startIndex == that.startIndex &&
                    endIndex == that.endIndex;
        }

        @Override
        public int hashCode() {
            return Objects.hash(startIndex, endIndex);
        }
    }

    public static class MatrixRow {
        private int[] internalData;

        private HashMap<IndexesTuple, Long> sumResults;

        public MatrixRow() {
            sumResults = new HashMap<>();
        }

        public MatrixRow(String inputData)
        {
            this();
            initRow(inputData);
        }

        public MatrixRow(int[] data)
        {
            this();
            internalData = data;
        }

        private void initRow(String inputData) {
            inputData = inputData.trim();
            String[] splittedData = inputData.split(" ");
            internalData = new int[splittedData.length];
            for (int i = 0; i < splittedData.length; i++) {
                internalData[i] = Integer.parseInt(splittedData[i]);
            }
        }

        public MatrixRow clone(){
            return new MatrixRow(Arrays.copyOf(internalData, internalData.length));
        }

        public int[] getData(){
            return internalData;
        }

        public int getValueAt(int index)
        {
            return internalData[index];
        }

        public void setNewValue(int index, int newValue)
        {
            internalData[index] = newValue;
            sumResults.clear();
        }

        public int getRowLenght(){
            if (internalData != null) {
                return internalData.length;
            }
            return 0;
        }

        public long getSumBetweenIndexes(int startIndex, int endIndex) {
            IndexesTuple tuple = new IndexesTuple(startIndex, endIndex);
            if (sumResults.containsKey(tuple)) {
                return sumResults.get(tuple);
            }
            long result = 0;
            for(int i = startIndex; i <=endIndex; i++) {
                result += internalData[i];
            }
            sumResults.put(tuple, result);
            return result;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MatrixRow row = (MatrixRow) o;
            return Arrays.equals(internalData, row.internalData);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(internalData);
        }
    }

    public static class Matrix{
        private HashMap<IndexesQuad, Long> calculatedQueries;

        private List<MatrixRow> rowsList;
        private int matrixSize;

        public Matrix() {
            calculatedQueries = new HashMap<>();
        }

        public Matrix(int matrixSize) {
            this();
            rowsList = new ArrayList<>();
            this.matrixSize = matrixSize;
        }

        public void addRow(MatrixRow newRow) throws SizeMismatchException {
            if (newRow.getRowLenght() != matrixSize) {
                throw new SizeMismatchException(matrixSize, newRow.getRowLenght());
            }
            rowsList.add(newRow);
        }

        public Matrix clone(){
            Matrix newMatrix = new Matrix(matrixSize);
            for (MatrixRow row: rowsList) {
                try {
                    newMatrix.addRow(row.clone());
                } catch (SizeMismatchException e) {
                    e.printStackTrace();
                }
            }
            return newMatrix;
        }

        public void modifyValue(int rowNumber, int columnNumber, int newValue) {
            MatrixRow row = rowsList.get(columnNumber);
            row.setNewValue(rowNumber, newValue);
            calculatedQueries.clear();
        }

        public long sumSubMatrix(int startRowNumber, int startColumnNumber, int endRowNumber, int endColumnNumber ) {
            IndexesQuad quad = new IndexesQuad(startRowNumber, startColumnNumber,endRowNumber, endColumnNumber);
            if (calculatedQueries.containsKey(quad)) {
                return calculatedQueries.get(quad);
            }
            long result = 0;
            for (int i = startRowNumber; i <= endRowNumber; i++) {
                result += rowsList.get(i).getSumBetweenIndexes(startColumnNumber, endColumnNumber);
            }
            calculatedQueries.put(quad, result);
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Matrix matrix = (Matrix) o;
            return matrixSize == matrix.matrixSize &&
                    Objects.equals(rowsList, matrix.rowsList);
        }

        @Override
        public int hashCode() {
            return Objects.hash(rowsList, matrixSize);
        }
    }

    public enum QueryType {
        Calculate,
        Modify
    }

    public static class ModifyElementQuery extends AbstractQuery {
        private int rowIndex;
        private int columnIndex;
        private int newValue;

        public ModifyElementQuery(int rowIndex, int columnIndex, int value) {
            this.rowIndex = rowIndex;
            this.columnIndex = columnIndex;
            this.newValue = value;
        }

        public QueryType getQueryType(){
            return QueryType.Modify;
        }

        public ModifyElementQuery(String inputData) throws IncorrectQueryTypeException {
            parseInputData(inputData);
        }

        private void parseInputData(String inputData) throws IncorrectQueryTypeException {

            String[] splittedData = inputData.trim().split(" ");
            if (!splittedData[0].equals("2")) {
                throw new IncorrectQueryTypeException();
            }
            this.rowIndex = Integer.parseInt(splittedData[1]);
            this.columnIndex = Integer.parseInt(splittedData[2]);
            this.newValue = Integer.parseInt(splittedData[3]);
        }

        @Override
        public String execute() {
            getInternalMatrix().modifyValue(rowIndex, columnIndex, newValue);return "";
        }
    }

    public static class CalculateSumQuery extends AbstractQuery {
        private int startRowIndex;
        private int startColumnIndex;
        private int endRowIndex;
        private int endColumnIndex;

        public CalculateSumQuery(int queryNumber, String inputData) throws IncorrectQueryTypeException {
            setQueryNumber(queryNumber);
            parseInputData(inputData);
        }

        public QueryType getQueryType(){
            return QueryType.Calculate;
        }

        public CalculateSumQuery(int startRowIndex, int startColumnIndex, int endRowIndex, int endColumnIndex) {
            this.startRowIndex = startRowIndex;
            this.startColumnIndex = startColumnIndex;
            this.endRowIndex = endRowIndex;
            this.endColumnIndex = endColumnIndex;
        }

        private void parseInputData(String inputData) throws IncorrectQueryTypeException {
            String[] splittedData = inputData.trim().split(" ");
            if (!splittedData[0].equals("1")) {
                throw new IncorrectQueryTypeException();
            }

            this.startRowIndex = Integer.parseInt(splittedData[2]);
            this.startColumnIndex = Integer.parseInt(splittedData[1]);
            this.endRowIndex = Integer.parseInt(splittedData[4]);
            this.endColumnIndex = Integer.parseInt(splittedData[3]);
        }

        @Override
        public String execute() {
            return String.valueOf(getInternalMatrix().sumSubMatrix(startRowIndex, startColumnIndex, endRowIndex, endColumnIndex));
        }
    }

    public static class TaskData {
        private List<String> matrixInputData;
        private List<String> queriesInputData;

        private int matrixSize;

        public int getMatrixSize() {
            return matrixSize;
        }

        public void setMatrixSize(int matrixSize) {
            this.matrixSize = matrixSize;
        }

        public List<String> getMatrixInputData() {
            return matrixInputData;
        }

        public void setMatrixInputData(List<String> matrixInputData) {
            this.matrixInputData = matrixInputData;
        }

        public List<String> getQueriesInputData() {
            return queriesInputData;
        }

        public void setQueriesInputData(List<String> queriesInputData) {
            this.queriesInputData = queriesInputData;
        }
    }

    public static class TaskDataHandler {

        private final int minMatrixRowsNumber = 1;
        private final int maxMatrixRowsNumber  = 550;

        private final int minQueriesNumber = 0;
        private final int maxQueriesNumber = 100100;

        private boolean isMatrixRowsQuantityCorrect(int currentRowsQuantity){
            return minMatrixRowsNumber <= currentRowsQuantity && currentRowsQuantity <= maxMatrixRowsNumber;
        }

        private boolean isQueriesQuantityCorrect(int queriesQuantity){
            return minQueriesNumber <= queriesQuantity && queriesQuantity<= maxQueriesNumber;
        }

        private List<String> getDataList(Scanner scanner, int currentRowsQuantity) {
            List<String> result = new ArrayList<>();
            int i = 0;
            while(i < currentRowsQuantity) {
                if (scanner.hasNextLine()) {
                    result .add(scanner.nextLine());
                }
                i++;
            }
            return result;
        }

        public TaskData getInputTaskData(InputStream inputDataStream){
            TaskData handledData = new TaskData();
            Scanner internalScanner = new Scanner(inputDataStream);
            int matrixColumnsQuantity = internalScanner.nextInt();
            handledData.setMatrixSize(matrixColumnsQuantity);
            internalScanner.nextLine();
            if (isMatrixRowsQuantityCorrect(matrixColumnsQuantity)) {
                handledData.setMatrixInputData(getDataList(internalScanner, matrixColumnsQuantity));
            }
            int queriesQuantity = internalScanner.nextInt();
            internalScanner.nextLine();
            if (isQueriesQuantityCorrect(queriesQuantity)) {
                handledData.setQueriesInputData(getDataList(internalScanner, queriesQuantity));
            }
            return handledData;
        }
    }

    public static class MatrixCreator {
        private final List<String> inputMatrixData;
        private int matrixSize;

        public MatrixCreator(TaskData data) {
            this.inputMatrixData = data.getMatrixInputData();
            this.matrixSize = data.getMatrixSize();
        }

        public Matrix createMatrix() throws SizeMismatchException {
            Matrix matrix = new Matrix(matrixSize);
            for (String data: inputMatrixData) {
                matrix.addRow(new MatrixRow(data));
            }
            return matrix;
        }
    }

    public static class QueriesListCreator {
        private int calculateQueryNumber;
        private final List<String> inputQueries;

        public QueriesListCreator(TaskData incomingData)
        {
            calculateQueryNumber = 0;
            inputQueries = incomingData.getQueriesInputData();
        }

        public List<AbstractQuery> getQueriesList() throws IncorrectQueryTypeException {
            List<AbstractQuery> result = new ArrayList<>();
            for (String data: inputQueries) {
                String queryTypeSymbol = data.substring(0,1);
                switch (queryTypeSymbol) {
                    case "1":
                        result.add(new CalculateSumQuery(calculateQueryNumber++, data));
                        break;
                    case "2":
                        result.add(new ModifyElementQuery(data));
                        break;
                }
            }
            return result;
        }
    }

    public static class CalculationThread extends RecursiveAction {
        private List<AbstractQuery> queries;
        private ConcurrentSkipListMap<Integer,String> listForResults;
        private Integer start;
        private Integer end;
        private final Integer THRESHOLD = 1000;

        public CalculationThread(ConcurrentSkipListMap<Integer,String> listForResults, Integer start, Integer end, List<AbstractQuery> queries) {
            this.start= start;
            this.end = end;
            this.listForResults = listForResults;
            this.queries = queries;
        }

        @Override
        public void compute() {
            if (end - start > THRESHOLD) {
                int middle = (end + start) / 2;

                CalculationThread firstThread = new CalculationThread(listForResults, start, middle, queries);
                CalculationThread secondThread = new CalculationThread(listForResults, middle + 1, end, queries);

                firstThread .fork();
                secondThread.fork();

                secondThread.join();
                firstThread.join();
            } else  {
                for (AbstractQuery query : queries) {
                    if (query.getQueryType() == QueryType.Calculate) {
                        listForResults.put(query.getQueryNumber(),query.execute()) ;
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws IOException, IncorrectQueryTypeException, InterruptedException {
        TaskDataHandler handler = new TaskDataHandler();
        TaskData handledData = handler.getInputTaskData(System.in);
        MatrixCreator creator = new MatrixCreator(handledData);
        Matrix matrix;
        try {
            matrix = creator.createMatrix();
        } catch (SizeMismatchException e) {
            e.printStackTrace();
            return;
        }
        QueriesListCreator queriesListCreator = new QueriesListCreator(handledData);
        List<AbstractQuery> queries = queriesListCreator.getQueriesList();
        List<AbstractQuery> queryForCalculations = new ArrayList<>();
        for (AbstractQuery item: queries) {
            if (item.getQueryType() == QueryType.Calculate) {
                item.setCurrentMatrix((Matrix) matrix.clone());
                queryForCalculations.add(item);
            }
            if (item.getQueryType() == QueryType.Modify) {
                item.setCurrentMatrix(matrix);
                item.execute();
            }
        }
        ConcurrentSkipListMap<Integer,String> results = new ConcurrentSkipListMap<>();
        ForkJoinPool pool = new ForkJoinPool();
        CalculationThread calculationThread = new CalculationThread(results,0, queryForCalculations.size(),queryForCalculations);
        pool.execute(calculationThread);
        calculationThread.join();
        while(!calculationThread.isDone()){
            Thread.sleep(10);
        }
        for(Map.Entry<Integer, String> entry : results.entrySet()){
            System.out.println(entry.getValue());
        }
    }

}
